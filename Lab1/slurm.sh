#!/bin/bash
##SBATCH --partition=nuc     ### Partition
#SBATCH --job-name=hello     ### Job Name
#SBATCH --time=1:00:00      ### WallTime
#SBATCH --nodes=1            ### Number of Nodes
#SBATCH --ntasks-per-node=4 ### Number of tasks (MPI processes)
##SBATCH --cpus-per-task=4

echo Hello from `hostname`
 
module load intel/17
module load mpi/intel-17
which mpirun

mpirun -np 4 ./a.out
